/* eslint-disable */
import Vue from 'vue';

Vue.component('markdown-pages', {
  template: '<div v-html="$md.render(pages[page])"></div>',
  props: {
    page: {
      type: String,
      required: true
    },
    content: {
      type: String,
      required: false
    }
  },
  data(){
    return {
      pages: {
<% _.forEach(options.pages, function(page) { %>
    <%= page.src %>: `<%= page.markdown %>`,
<% }); %>
      }
    }
  }
});
