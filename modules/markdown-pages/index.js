import path from 'path';
import fs from 'fs';
import readdir from 'recursive-readdir';

export default function asyncModule(moduleOptions) {
  return new Promise((resolve, reject) => {
    const self = this;
    const options = Object.assign(
      {},
      self.options.markdownPages,
      moduleOptions
    );
    const contentDir = path.resolve(self.options.rootDir, 'content');
    readdir(contentDir)
      .then((files) => {
        const pages = {};
        files.forEach((file) => {
          let parentObj = null;
          file
            .split(contentDir)[1]
            .split(path.sep)
            .slice(1)
            .forEach((item) => {
              self.options.generate.routes.push('wiki/' + item.split('.md')[0]);
              if (item.includes('.md')) {
                if (parentObj !== null) {
                  pages[parentObj][item.split('.md')[0]] = {
                    src: item.split('.md')[0],
                    markdown: fs.readFileSync(file, { encoding: 'utf8' })
                  };
                } else {
                  pages[item.split('.md')[0]] = {
                    src: item.split('.md')[0],
                    markdown: fs.readFileSync(file, { encoding: 'utf8' })
                  };
                }
                parentObj = null;
              } else {
                parentObj = item;
                if (
                  pages[parentObj] === undefined ||
                  pages[parentObj] === null
                ) {
                  pages[parentObj] = {};
                }
              }
            });

          // console.log(page);
        });
        // console.log(pages);
        self.addPlugin({
          src: path.resolve(__dirname, 'plugin.js'),
          options: {
            pages
          }
        });
        resolve();
      })
      .catch((error) => {
        reject(error);
      });

    self.nuxt.hook('build:compile', async ({ name, compiler }) => {
      // Called before the compiler (default: webpack) starts
      // console.log('Hello');
    });
  });
}
