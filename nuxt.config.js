import colors from 'vuetify/es5/util/colors';

export default {
  mode: 'spa',
  generate: {
    dir: 'public'
  },
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: '%s - Troll Engine',
    title: 'Home',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      },
      { name: 'msapplication-TileColor', content: '#2b5797' },
      { name: 'msapplication-TileImage', content: '/mstile-144x144.png' },
      { name: 'theme-color', content: '#424242' }
    ],
    link: [
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/apple-touch-icon.png'
      },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/favicon-32x32.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicon-16x16.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '192x192',
        href: '/android-chrome-192x192.png'
      },
      { rel: 'mask-icon', color: '#2b5797', href: '/safari-pinned-tab.svg' }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['~/assets/less/main.less'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/vuetify.js'],
  /*
   ** Nuxt.js dev-modules
   */
  markdownit: {
    injected: true
  },

  pwa: {
    workbox: {
      autoRegister: true,
      offline: true,
      offlineStrategy: 'StaleWhileRevalidate'
    },
    manifest: {
      name: 'Troll Engine',
      short_name: 'troll engine',
      theme_color: '#001317',
      background_color: '#ffffff',
      homepage_url: 'https://troll-engine.com',
      start_url: 'https://troll-engine.com',
      display: 'standalone',
      icons: [
        {
          src: '/android-chrome-36x36.png',
          sizes: '36x36',
          type: 'image/png'
        },
        {
          src: '/android-chrome-48x48.png',
          size: '48x48',
          type: 'image/png'
        },
        {
          src: '/android-chrome-72x72.png',
          sizes: '72x72',
          type: 'image/png'
        },
        {
          src: '/android-chrome-96x96.png',
          sizes: '96x96',
          type: 'image/png'
        },
        {
          src: '/android-chrome-144x144.png',
          sizes: '144x144',
          type: 'image/png'
        },
        {
          src: '/android-chrome-192x192.png',
          sizes: '192x192',
          type: 'image/png'
        },
        {
          src: '/android-chrome-256x256.png',
          sizes: '256x256',
          type: 'image/png'
        },
        {
          src: '/android-chrome-384x384.png',
          sizes: '384x384',
          type: 'image/png'
        },
        {
          src: '/android-chrome-512x512.png',
          sizes: '512x512',
          type: 'image/png'
        }
      ]
    }
  },
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
    '~/modules/markdown-pages/index.js'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    [
      '@nuxtjs/pwa',
      {
        meta: false,
        icon: false,
        onesignal: false
      }
    ],
    '@nuxtjs/markdownit'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      config.resolve.alias.vue = 'vue/dist/vue.common';
      if (ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        });
      }
    }
  }
};
